<?php

echo "<fieldset><legend> Operator Aritmatika</legend>";
$a = 5;
$b = 2;
//penjumlahan
$c = $a + $b;
echo "$a + $b = $c";
echo "<hr>"; //membentuk garis 

//pengurangan
$c = $a - $b;
echo "$a - $b = $c";
echo "<hr>";

//perkalian
$c = $a * $b;
echo "$a * $b = $c";
echo "<hr>";

//pembagian
$c = $a / $b;
echo "$a / $b = $c";
echo "<hr>";

//modulus atau sisa bagi
$c = $a % $b;
echo "$a % $b = $c";
echo "<hr>";

//pangkat
$c = $a ** $b;
echo "$a ** $b = $c";
echo "<hr>";
echo "</fieldset>";

echo "<fieldset><legend> Operator Penugasan</legend>";
$jumlah = 50;
$jumlah = $jumlah + 10;
$jumlah += 20;
echo $jumlah;
echo "<hr>";

$jumlah -= 5;
echo $jumlah;
echo "<hr>";

$jumlah *= 2;
echo $jumlah;
echo "</fieldset>";

echo "<fieldset><legend> Operator Increment & Decrement</legend>";
$jumlah = 0;

$jumlah++;
echo $jumlah;
echo "<hr>";

$jumlah++;
echo $jumlah;
echo "<hr>";

$jumlah--;
echo $jumlah;
echo "</fieldset>";

echo "<fieldset><legend> Operator Relasi</legend>";
//menghasilakan tipe data boolean;
// 0 = false
// 1 = true
$a = 6;
$b = 2;

//menggunakan operator relasi lebih besar
$c = $a > $b;
echo "$a > $b = $c";
echo "<hr>";

//menggunakan operator relasi lebih kecil
$c = $a < $b;
echo "$a < $b = $c";
echo "<hr>";

//menggunakan operator relasi sama dengan
$c = $a == $b;
echo "$a == $b = $c";
echo "<hr>";

//menggunakan operator relasi tidak sama dengan
$c = $a != $b;
echo "$a != $b = $c";
echo "<hr>";

//menggunakan operator relasi lbh besar sama dengan
$c = $a >= $b;
echo "$a >= $b = $c";
echo "<hr>";

//menggunakan operator relasi lbh kecil sama dengan
$c = $a <= $b;
echo "$a <= $b = $c";
echo "<hr>";

echo "</fieldset>";

echo "<fieldset><legend> Operator Logika</legend>";
$a = true;
$b = false;

$c = $a && $b;
printf("%b && %b = %b", $a, $b, $c);
echo "<hr>";

$c = $a || $b;
printf("%b || %b = %b", $a, $b, $c);
echo "<hr>";

$c = !$a;
printf("!%b = %b", $a, $c);
echo "<hr>";

echo "</fieldset>";

echo "<fieldset><legend> Operator Ternary</legend>";

$suka = true;
$jawab = $suka ? "ya" : "tidak";
echo $jawab;
echo "<hr>";

$jkel = 0; //false
echo $jkel == 1 ? "laki-laki" : "perempuan";

echo "</fieldset>";
