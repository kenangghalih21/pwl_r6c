<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Gaji Karyawan</title>
</head>

<body>
    <h2>Form Gaji Karyawan</h2>
    <form action="proses.php" method="post">
        <table>
            <tr>
                <td>Kd_gaji</td>
                <td>:</td>
                <td> <input type="text" name="kd_gaji"> </td>
            </tr>
            <tr>
                <td>NIP</td>
                <td>:</td>
                <td> <input type="text" name="nip"> </td>
            </tr>
            <tr>
                <td>Bulan</td>
                <td>:</td>
                <td>
                    <select name="bulan">
                        <option value="Januari">Januari</option>
                        <option value="Februari">Februari</option>
                        <option value="Maret">Maret</option>
                        <option value="April">April</option>
                        <option value="Mei">Mei</option>
                        <option value="Juni">Juni</option>
                        <option value="Juli">Juli</option>
                        <option value="Agustus">Agustus</option>
                        <option value="September">September</option>
                        <option value="Oktober">Oktober</option>
                        <option value="November">November</option>
                        <option value="Desember">Desember</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Gaji Pokok</td>
                <td>:</td>
                <td> <input type="number" name="gapok"> </td>
            </tr>
            <tr>
                <td>Jabatan</td>
                <td>:</td>
                <td>
                    <select name="jabatan">
                        <option value="direktur">Direktur</option>
                        <option value="manajer">Manajer</option>
                        <option value="pegawai">Pegawai</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" name="simpan" value="Simpan"> </td>
                <td></td>
                <td> </td>
            </tr>
        </table>
    </form>
</body>

</html>
