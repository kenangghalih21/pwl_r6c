<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>form </title>
</head>

<body>
   <h3>FORM BIODATA</h3>
   <hr>
   <!-- nim genap -->
   <form action="form_aksi.php" method="POST">
      <table>
         <tr>
            <td> <label>Nama</label> </td>
            <td>:</td>
            <td>
               <input type="text" name="b">
            </td>
         </tr>
         <tr>
            <td> <label>Gender</label> </td>
            <td>:</td>
            <td>
               <input type="radio" name="c" value="laki-laki">laki-laki <br>
               <input type="radio" name="c" value="perempuan">perempuan
            </td>
         </tr> <br>
         <tr>
            <td> <label>Hobby</label> </td>
            <td>:</td>
            <td>
               <input type="checkbox" name="d[]" value="Korespondensi">Korespondensi <br>
               <input type="checkbox" name="d[]" value="Traveling">Traveling <br>
               <input type="checkbox" name="d[]" value="Shoping">Shoping
            </td>
         </tr>
         <tr>
            <td> <label>Pendidikan</label> </td>
            <td>:</td>
            <td>
               <select name="e">
                  <?php $arrpend = ['SD', 'SMP', 'SMA', 'S1'];
                  for ($i = 0; $i < count($arrpend); $i++) {
                     echo "<option value=$arrpend[$i]>" . $arrpend[$i] . "</option>";
                  } ?> </select>
            </td>
         </tr>
         <tr>
            <td> <label>Alamat</label> </td>
            <td>:</td>
            <td>
               <textarea type="text" name="f"></textarea>
            </td>
         </tr>
         <tr>
            <td></td>
            <td></td>
            <td>
               <input type="submit" value="simpan" name="simpan">
            </td>
         </tr>
      </table>
   </form>
</body>

</html>