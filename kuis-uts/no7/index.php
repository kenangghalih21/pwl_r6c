<html>

<head>
   <meta charset="UTF-8">
   <title>Perhitungan Laba Menggunakan PHP</title>
</head>

<body>
   <h2>Perhitungan Laba</h2>
   <form method="post" action="proses.php">
      <table>
         <tr>
            <th>Tepung Terigu</th>
            <td>Jmlh beli</td>
            <td><input type="number" step="0.01" name="htepung" required></td>
            <td>/kg</td>
            <td>(harga Rp 7.500/Kg)</td>
         </tr>
         <tr>
            <th>Gula Pasir</th>
            <td>Jmlh beli</td>
            <td><input type="number" step="0.01" name="hgula" required></td>
            <td>/kg</td>
            <td>(harga Rp 9.000/Kg)</td>
         </tr>
         <tr>
            <th>Margarin</th>
            <td>Jmlh beli</td>
            <td><input type="number" step="0.01" name="hmargarin" required></td>
            <td>/bungkus</td>
            <td>(harga Rp 5000/Bungkus)</td>
         </tr>
         <tr>
            <th>Telor</th>
            <td>Jmlh beli</td>
            <td><input type="number" step="0.01" name="htelor" required></td>
            <td>/kg</td>
            <td>(harga Rp 20.000/Kg)</td>
         </tr>
         <tr>
            <th>Minyak Goreng</th>
            <td>Jmlh beli</td>
            <td><input type="number" step="0.01" name="hminyak" required></td>
            <td>/L</td>
            <td>(harga Rp 11000/Buah)</td>
         </tr>
         <tr>
            <th>Jumlah Donat Terjual</th>
            <td>Banyaknya</td>
            <td><input type="number" step="0.01" name="jmldonat" required></td>
            <td>buah</td>
            <td>(harga Rp 2000/Buah)</td>
         </tr>
         <tr>
            <td><input type="submit" value="Hitung!"></td>
         </tr>
      </table>
   </form>
</body>

</html>