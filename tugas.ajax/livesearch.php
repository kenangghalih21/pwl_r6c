<?php
include("config.php");
if (isset($_POST['input'])) {

   $input = $_POST['input'];
   
   $query = "SELECT * FROM searchbarang WHERE nama_barang LIKE '{$input}%' OR stok LIKE '{$input}%' LIMIT 5";

   $result = mysqli_query($con, $query);

   if (mysqli_num_rows($result) > 0) { ?>
      <table class="table table-bordered table-striped mt-4 bg-warning">
         <thead>
            <tr>
               <th>Id</th>
               <th>Nama Barang</th>
               <th>Stok Barang</th>
            </tr>
         </thead>

         <tbody>
            <?php

            while ($row = mysqli_fetch_assoc($result)) {
               $id = $row['id'];
               $nama_barang = $row['nama_barang'];
               $stok = $row['stok'];
            ?>

               <tr>
                  <td><?php echo $id; ?></td>
                  <td><?php echo $nama_barang; ?></td>
                  <td><?php echo $stok; ?></td>
               </tr>

            <?php
            }
            ?>
         </tbody>
      </table>

<?php
   } else {
      echo "<h6 class='text-danger text-center mt-3'>Data tidak ditemukan</h6>";
   }
}
?>