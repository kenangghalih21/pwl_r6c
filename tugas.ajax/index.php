<!DOCTYPE html>
<html>

<head>
   <title>Lab.Komp Unindra</title>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

   <script defer src="https://use.fontawesome.com/releases/v5.8.1/js/all.js" integrity="sha384-g5uSoOSBd7KkhAMlnQILrecXvzst9TdC09/VM+pjDTCM+1il8RHz5fKANTFFb+gQ" crossorigin="anonymous"></script>

   <link rel="icon" href="img/logo.png" type="image/icon">

   <style>
      .container {
         background-color: rgba(255, 255, 128, .5);
      }
   </style>
</head>

<body>
   <div class="container">
      <nav class="navbar navbar-light bg-warning">
         <img id="logo" src="img/logo.png" width="100px" />
         <div class="text-center mt-5 mb-4">
            <h2>Pencarian Data Inventori Lab. Komputer Unindra</h2>
         </div>
         <button class="btn btn-primary" id="cart">
            <i class="fas fa-list"></i>
         </button>
      </nav>

      <div class="container" style="max-width:50%;">

         <input type="text" class="form-control mt-5" id="live_search" autocomplete="off" placeholder="Ketik untuk mencari barang...">
      </div>

      <div id="searchresult"></div>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

      <script type="text/javascript">
         $(document).ready(function() {

            $("#live_search").keyup(function() {
               var input = $(this).val();
               //alert(input);

               if (input != "") {
                  $.ajax({
                     url: "livesearch.php",
                     method: "POST",
                     data: {
                        input: input
                     },
                     success: function(data) {
                        $("#searchresult").html(data);
                        $("#searchresult").css("display", "block");
                     }
                  });
               } else {
                  $("#searchresult").css("display", "none");
               }
            });
         });
      </script>
   </div>
</body>

</html>